﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

namespace Bubble
{
    using Interaction;
    using Messaging;

    public class Director: MonoBehaviour
    {
        AudioSource sound;
        TextMesh text;

        void Awake()
        {
            sound = GetComponentInChildren<AudioSource>();
            text = GetComponentInChildren<TextMesh>();
        }

        void Update()
        {
            if (Input.GetButtonUp("Cancel")) {
                Application.Quit();
            }
        }

        void OnEnable()
        {
            MessageBus.Subscribe<LevelEndMessage>(OnLevelEnd);   
        }

        void OnDisable()
        {
            MessageBus.Unsubscribe<LevelEndMessage>(OnLevelEnd);   
        }

        void OnLevelEnd(LevelEndMessage message)
        {
            if (message.successful)
                sound.Play();
            StartCoroutine(LoadNextLevel(message.nextLevel, message.successful));
        }

        IEnumerator LoadNextLevel(int nextScene, bool successful)
        {
            float waitTime = successful ? 2.0f : 1.0f;
            yield return new WaitForSeconds(waitTime);
            SceneManager.LoadScene(nextScene);
            float elapsed = 0.0f;
            while (elapsed < 3.0f) {
                elapsed += Time.deltaTime;
                float alpha = elapsed / 3.0f;
                text.color = new Color(0, 0, 0, alpha);
                yield return null;
            }
        }
    }
}