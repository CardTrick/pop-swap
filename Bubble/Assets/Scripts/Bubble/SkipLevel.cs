using UnityEngine;
using UnityEngine.SceneManagement;

namespace Bubble
{
    public class SkipLevel: MonoBehaviour
    {
        void Start()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    }
}