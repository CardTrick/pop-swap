using UnityEngine;

namespace Bubble.Messaging
{
    public delegate void OnMessageReceived<T>(T message) where T: Message;

    public class MessageBus<T> where T: Message
    {
        public static MessageBus<T> mainBus = new MessageBus<T>();

        public event OnMessageReceived<T> MessageEvent;


        public void SubscribeMessage(OnMessageReceived<T> messageHandler)
        {
            MessageEvent += messageHandler;
        }

        public void UnsubscribeMessage(OnMessageReceived<T> messageHandler)
        {
            MessageEvent -= messageHandler;
        }

        public void PublishMessage(T message)
        {
            if (MessageEvent != null)
                MessageEvent(message);
        }

        public static void Subscribe(OnMessageReceived<T> messageHandler)
        {
            mainBus.SubscribeMessage(messageHandler);
        }

        public static void Unsubscribe(OnMessageReceived<T> messageHandler)
        {
            mainBus.UnsubscribeMessage(messageHandler);
        }

        public static void Publish(T message)
        {
            mainBus.PublishMessage(message);
        }
    }

    public static class MessageBus
    {
        public static void Subscribe<T>(OnMessageReceived<T> messageHandler) where T: Message
        {
            MessageBus<T>.Subscribe(messageHandler);
        }

        public static void Unsubscribe<T>(OnMessageReceived<T> messageHandler) where T: Message
        {
            MessageBus<T>.Unsubscribe(messageHandler);   
        }

        public static void Publish<T>(T message) where T: Message
        {
            MessageBus<T>.Publish(message);
        }
    }
}