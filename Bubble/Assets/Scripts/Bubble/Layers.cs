﻿using UnityEngine;

namespace Bubble
{
    public static class Layers
    {
        public const int Default = 0;
        public const int TransparentFX = 1;
        public const int IgnoreRaycast = 2;
        public const int Water = 4;
        public const int UI = 5;
        public const int Creature = 8;
        public const int Bubble = 9;
        public const int NonBouncy = 10;
        public const int Glow = 11;
    }

    public static class Masks
    {
        public const int Raycast = ~(1 << Layers.TransparentFX);
        
        public const int Default = 1 << Layers.Default;
        public const int TransparentFX = 1 << Layers.TransparentFX;
        public const int IgnoreRaycast = 1 << Layers.IgnoreRaycast;
        public const int Water = 1 << Layers.Water;
        public const int UI = 1 << Layers.UI;
        public const int Creature = 1 << Layers.Creature;
        public const int Bubble = 1 << Layers.Bubble;
        public const int NonBouncy = 1 << Layers.NonBouncy;
        public const int Glow = 1 << Layers.Glow;
    }
}