﻿using UnityEngine;

namespace Bubble.Fans
{
    using Interaction;

    public class Fan: Switchable
    {
        public bool on = false;
        public float height = 0.0f;
        public float fanPropulsion = 10.0f;

        GameObject wind;
        RaycastHit[] hits = new RaycastHit[16];

        void OnDrawGizmos()
        {
            Gizmos.color = (on) ? Color.yellow : Color.yellow * 0.5f;
            Gizmos.DrawLine(transform.position, transform.position + transform.up * height);
        }

        public override bool switched { get { return on; } }

        public override void Switch(bool active)
        {
            on = active;
        }

        void Awake()
        {
            var particles = GetComponentInChildren<ParticleSystem>();
            particles.startSpeed = fanPropulsion;
            particles.startLifetime = height / particles.startSpeed;
            wind = particles.gameObject;
            wind.SetActive(on);
        }

        void Update()
        {
            if (wind.activeSelf != on)
                wind.SetActive(on);
        }

        void FixedUpdate()
        {
            if (!on)
                return;

            Vector3 center = transform.position + transform.up * 0.25f;
            Vector3 extents = new Vector3(0.5f, 0.5f, 0.95f);
            int numhits = Physics.BoxCastNonAlloc(
                center, extents, transform.up, hits,
                Quaternion.identity, height, Masks.Bubble);
            for (int i=0; i<numhits; ++i) {
                var hit = hits[i];
                PropelObject(hit);
            }
        }

        void PropelObject(RaycastHit hit)
        {
            var body = hit.rigidbody;
            if (body) {
                body.AddForce(transform.up * fanPropulsion, ForceMode.Acceleration);
            }
        }
    }
}