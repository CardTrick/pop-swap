﻿using UnityEngine;

namespace Bubble.Fans
{
    using Nesting;

    public class PropellorSpin: MonoBehaviour
    {
        public float fanSpeed = 90.0f;
        public float acceleration = 90.0f;

        AudioSource sound;
        Fan fan;        

        float speed = 0.0f;

        void Awake()
        {
            var root = GetComponentInParent<Root>();
            fan = root.GetComponentInChildren<Fan>();
            sound = root.GetComponentInChildren<AudioSource>();
        }

        void Update()
        {
            float delta = (fan.on) ? Time.deltaTime : -Time.deltaTime;
            delta *= acceleration;
            speed += delta;
            speed = Mathf.Clamp(speed, 0, fanSpeed);
            sound.volume = speed / fanSpeed;
            transform.Rotate(-Vector3.up * speed * Time.deltaTime);
        }
    }
}