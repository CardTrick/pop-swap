using UnityEngine;

namespace Bubble.Nesting
{
    public class CompositePrefab: MonoBehaviour
    {
        public GameObject rootPrefab;
        public GameObject[] childPrefabs;

        void Awake()
        {
            var root = AddChild(rootPrefab, transform);
            foreach (var childPrefab in childPrefabs)
                AddChild(childPrefab, root);

            Initialize();
        }

        Transform AddChild(GameObject prefab, Transform root)
        {
            var childObj = Instantiate(prefab) as GameObject;
            var child = childObj.transform;
            child.SetParent(root);
            child.localPosition = Vector3.zero;
            child.localRotation = Quaternion.identity;
            child.localScale = Vector3.one;
            return child;
        }

        void Initialize()
        {
            BroadcastMessage("Wire", SendMessageOptions.DontRequireReceiver);
            BroadcastMessage("Init", SendMessageOptions.DontRequireReceiver);
        }
    }
}