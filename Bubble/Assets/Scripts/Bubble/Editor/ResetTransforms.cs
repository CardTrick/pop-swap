using UnityEngine;
using UnityEditor;

namespace Bubble
{
    namespace Editor
    {
        public class ResetTransforms: ScriptableObject
        {

            [MenuItem("Waltz/Reset Placement %M", false, 0)]
            public static void ResetPlacement()
            {
                Transform[] selected = Selection.transforms;
                foreach (Transform transform in selected) {
                    Undo.RecordObject(transform, "Reset Placement");
                    transform.localPosition = Vector3.zero;
                    transform.localRotation = Quaternion.identity;
                }
            }

            [MenuItem("Waltz/Reset Transform %#M", false, 1)]
            public static void ResetTransform()
            {
                Transform[] selected = Selection.transforms;
                foreach (Transform transform in selected) {
                    Undo.RecordObject(transform, "Reset Transform");
                    transform.localPosition = Vector3.zero;
                    transform.localRotation = Quaternion.identity;
                    transform.localScale = Vector3.one;
                }
            }
        }
    }
}
