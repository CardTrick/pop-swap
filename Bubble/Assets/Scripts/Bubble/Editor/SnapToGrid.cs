using UnityEngine;
using UnityEditor;

namespace Bubble
{
    namespace Editor
    {
        public class SnapToGrid: ScriptableObject
        {

            [MenuItem("Waltz/Snap to Unit Grid %G", false, 10)]
            public static void SnapToUnitGrid()
            {
                Transform[] selected = Selection.transforms;
                SnapSelectedToArbitraryGrid(selected, 1.0f);
            }

            [MenuItem("Waltz/Snap to Half-unit Grid %#G", false, 11)]
            public static void SnapToHalfUnitGrid()
            {
                Transform[] selected = Selection.transforms;
                SnapSelectedToArbitraryGrid(selected, 0.5f);
            }

            static void SnapSelectedToArbitraryGrid(Transform[] selected, float gridSize)
            {
                foreach (var transform in selected)
                    SnapTransformToArbitraryGrid(transform, gridSize);
            }

            static void SnapTransformToArbitraryGrid(Transform transform, float gridSize)
            {
                Undo.RecordObject(transform, "Snap To Grid");
                var scale = SnapVector(transform.localScale, gridSize);
                var gridPos = SnapVector(transform.position, gridSize);
                var halfPos = SnapVector(transform.position, gridSize / 2);
                var offset = ScaleOffset(scale, gridSize);
                var pos = new Vector3(
                    Mathf.Lerp(gridPos.x, halfPos.x, offset.x),
                    Mathf.Lerp(gridPos.y, halfPos.y, offset.y),
                    Mathf.Lerp(gridPos.z, halfPos.z, offset.z));
                transform.position = pos;
                transform.localScale = scale;
            }

            static Vector3 SnapVector(Vector3 vec, float gridSize)
            {
                var scaledVec = vec * (1.0f / gridSize);
                var snappedVec = new Vector3(
                    Mathf.Round(scaledVec.x), Mathf.Round(scaledVec.y), Mathf.Round(scaledVec.z));
                var unscaledVec = snappedVec * gridSize;
                return unscaledVec;
            }

            static Vector3 ScaleOffset(Vector3 vec, float gridSize)
            {
                var scaledVec = vec * (1.0f / gridSize);
                int gx = Mathf.RoundToInt(scaledVec.x);
                int gy = Mathf.RoundToInt(scaledVec.y);
                int gz = Mathf.RoundToInt(scaledVec.z);
                return new Vector3(Offset(gx), Offset(gy), Offset(gz));
                
            }

            static float Offset(int gridScale)
            {
                return (gridScale % 2 == 0) ? 1.0f : 0.0f;
            }
        }
    }
}
