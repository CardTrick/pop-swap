using UnityEngine;

namespace Bubble.Shapeshifting
{
    using Messaging;

    public class ShapeshiftController: MonoBehaviour
    {
        public string shapeshiftButton = "Shapeshift";

        public float coolDown = 1.0f;
        float coolingDown = 0.0f;

        void Update()
        {
            if (coolingDown > 0)
                coolingDown -= Time.deltaTime;
            else
                CheckPressed();
        }

        void CheckPressed()
        {
            if (Input.GetButtonDown(shapeshiftButton))
                Shapeshift();
        }

        void Shapeshift()
        {
            MessageBus.Publish(new ShapeshiftMessage());
            coolingDown = coolDown;
        }
    }
}