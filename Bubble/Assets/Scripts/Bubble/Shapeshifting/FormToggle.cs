using UnityEngine;

namespace Bubble.Shapeshifting
{
    public class FormToggle: Shapeshifty
    {
        Transform initialForm;
        Transform secondaryForm;

        public bool startWithSecondaryForm = false;
        public bool syncPositions = true;

        bool toggled;

        void Start()
        {
            initialForm = transform.GetChild(0);
            secondaryForm = transform.GetChild(1);
            toggled = startWithSecondaryForm;
            Shift();
        }

        void Shift()
        {
            if (syncPositions) {
                var previousForm = (!toggled) ? secondaryForm : initialForm; 
                var currentForm = (toggled) ? secondaryForm : initialForm;
                var position = previousForm.transform.position;
                currentForm.transform.position = position;
            }
            initialForm.gameObject.SetActive(!toggled);
            secondaryForm.gameObject.SetActive(toggled);
        }

        protected override void OnShapeshift()
        {
            toggled = !toggled;
            Shift();
        }
    }
}