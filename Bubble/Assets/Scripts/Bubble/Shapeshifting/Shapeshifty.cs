﻿using UnityEngine;

namespace Bubble.Shapeshifting
{
    using Messaging;

    public abstract class Shapeshifty: MonoBehaviour
    {
        void OnEnable()
        {
            MessageBus.Subscribe<ShapeshiftMessage>(OnShapeshiftMessage);
        }

        void OnDisable()
        {
            MessageBus.Unsubscribe<ShapeshiftMessage>(OnShapeshiftMessage);
        }

        void OnShapeshiftMessage(ShapeshiftMessage message)
        {
            OnShapeshift();
        }

        protected abstract void OnShapeshift();
    }
}
