using UnityEngine;

namespace Bubble.Interaction
{
    public class Button: Switchable
    {
        public Switchable switchable;

        public override bool switched { get { return switchable.switched; } }

        public override void Switch(bool active)
        {
            switchable.Switch(active);
        }

        void OnDrawGizmos()
        {
            if (!switchable)
                return;
            Gizmos.color = Color.green;
            Gizmos.DrawLine(transform.position, switchable.transform.position);
        }
    }
}