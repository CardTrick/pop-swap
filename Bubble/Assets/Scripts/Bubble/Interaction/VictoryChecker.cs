using UnityEngine;
using UnityEngine.SceneManagement;

namespace Bubble.Interaction
{
    using Messaging;
    using Nesting;

    public class VictoryChecker: MonoBehaviour
    {
        Doorway doorway;
        bool done = false;

        void Awake()
        {
            var root = GetComponentInParent<Root>();
            doorway = root.GetComponentInChildren<Doorway>();
        }

        void Update()
        {
            if (done)
                return;

            if (doorway.bothPlayersIn)
            {
                Victory();
            }

            if (Input.GetButtonUp("Restart")) {
                RestartLevel();
            }
        }

        void Victory()
        {
            done = true;
            Scene scene = SceneManager.GetActiveScene();
            int nextScene = scene.buildIndex + 1;
            MessageBus.Publish(new LevelEndMessage(nextScene, true));
        }

        void RestartLevel()
        {
            done = true;
            Scene scene = SceneManager.GetActiveScene();
            int thisScene = scene.buildIndex;
            MessageBus.Publish(new LevelEndMessage(thisScene, false));   
        }
    }
}