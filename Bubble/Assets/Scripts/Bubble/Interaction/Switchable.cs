﻿using UnityEngine;

namespace Bubble.Interaction
{
    public abstract class Switchable: MonoBehaviour
    {
        public abstract void Switch(bool active);
        public abstract bool switched { get; }

        public void Toggle()
        {
            Switch(!switched);
        }
    }
}