using UnityEngine;

namespace Bubble.Interaction
{
    using Nesting;
    using Players;

    public class Doorway: MonoBehaviour
    {
        public bool firstPlayerIn { get; private set; }
        public bool secondPlayerIn { get; private set; }
        public bool bothPlayersIn { get; private set; }

        bool firstPlayerDetected = false;
        bool secondPlayerDetected = false;

        AudioSource sound;

        void Awake()
        {
            sound = GetComponentInChildren<AudioSource>();
        }

        void FixedUpdate()
        {
            if (firstPlayerDetected && !firstPlayerIn) {
                sound.Play();
            }
            firstPlayerIn = firstPlayerDetected;
            if (secondPlayerDetected && !secondPlayerIn) {
                sound.Play();
            }
            secondPlayerIn = secondPlayerDetected;
            if (firstPlayerIn && secondPlayerIn)
                bothPlayersIn = true;
            firstPlayerDetected = false;
            secondPlayerDetected = false;
        }

        void OnTriggerStay(Collider other)
        {
            var player = GetPlayerIdentity(other);
            if (player == PlayerIdentity.FirstPlayer)
                firstPlayerDetected = true;
            else if (player == PlayerIdentity.SecondPlayer)
                secondPlayerDetected = true;
        }

        PlayerIdentity GetPlayerIdentity(Collider other)
        {
            var root = other.GetComponentInParent<Root>();
            if (!root)
                return PlayerIdentity.Invalid;

            var player = root.GetComponentInChildren<Player>();
            if (player)
                return player.playerIdentity;
            return PlayerIdentity.Invalid;
        }
    }
}