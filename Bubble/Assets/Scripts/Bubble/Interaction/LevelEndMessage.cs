namespace Bubble.Interaction
{
    using Messaging;
    
    public class LevelEndMessage: Message
    {
        public readonly int nextLevel;
        public readonly bool successful;

        public LevelEndMessage(int nextLevel, bool successful)
        {
            this.nextLevel = nextLevel;
            this.successful = successful;
        }
    }
}