﻿using UnityEngine;

namespace Bubble.Interaction
{
    public class Bouncy: MonoBehaviour
    {
        Vector3 bounceDirection { get { return transform.up; } }

        public float bounceSpeed = 15.0f;
        public float awayBounciness = 0.75f;

        void OnTriggerEnter(Collider other)
        {
            var body = other.attachedRigidbody;
            bool nonbouncy = other.gameObject.layer == Layers.NonBouncy;
            if (body && !nonbouncy)
                BounceObject(body);
        }

        void BounceObject(Rigidbody body)
        {
            var normal = (body.transform.position - transform.position).normalized;
            float bounceCorrectness = Vector3.Dot(bounceDirection, normal);
            if (bounceCorrectness > 0)
                BounceUp(body);
            else
                BounceAway(body, normal);
        }

        void BounceUp(Rigidbody body)
        {
            var velocity = body.velocity;
            velocity -= Vector3.Project(velocity, bounceDirection);
            velocity += bounceDirection * bounceSpeed;
            body.velocity = velocity;
        }

        void BounceAway(Rigidbody body, Vector3 normal)
        {
            float speed = body.velocity.magnitude;
            body.velocity = normal * speed * awayBounciness;
        }
    }
}