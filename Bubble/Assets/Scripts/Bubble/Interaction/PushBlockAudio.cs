﻿using UnityEngine;

namespace Bubble.Interaction
{
    public class PushBlockAudio: MonoBehaviour
    {
        public float maxSpeed = 5.0f;

        AudioSource sound;
        Rigidbody body;

        void Awake()
        {
            sound = GetComponentInChildren<AudioSource>();
            body = GetComponentInChildren<Rigidbody>();
        }

        void Update()
        {
            float volume = Mathf.Abs(body.velocity.x) / maxSpeed;
            Mathf.Clamp01(volume);
            if (Mathf.Abs(body.velocity.y) > 0.05f)
                volume = 0;
            sound.volume = volume;
        }
    }
}