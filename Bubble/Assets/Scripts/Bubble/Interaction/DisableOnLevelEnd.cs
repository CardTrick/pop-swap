using UnityEngine;

namespace Bubble.Interaction
{
    using Messaging;

    public class DisableOnLevelEnd: MonoBehaviour
    {
        void OnEnable()
        {
            MessageBus.Subscribe<LevelEndMessage>(OnLevelEnd);
        }

        void OnDisable()
        {
            MessageBus.Unsubscribe<LevelEndMessage>(OnLevelEnd);
        }

        void OnLevelEnd(LevelEndMessage message)
        {
            gameObject.SetActive(false);
        }
    }
}