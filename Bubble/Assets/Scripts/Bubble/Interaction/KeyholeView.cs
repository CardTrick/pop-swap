﻿using UnityEngine;

namespace Bubble.Interaction
{
    using Nesting;
    using Players;

    public class KeyholeView: MonoBehaviour
    {
        public PlayerIdentity playerIdentity;

        Doorway doorway;
        Renderer view;
        bool visible;

        void Awake()
        {
            var root = GetComponentInParent<Root>();
            doorway = root.GetComponentInChildren<Doorway>();
            view = GetComponent<Renderer>();
            SetVisible(false);
        }

        void Update()
        {
            bool nowVisible = (playerIdentity == PlayerIdentity.FirstPlayer) ? doorway.firstPlayerIn : doorway.secondPlayerIn;
            if (nowVisible != visible)
                SetVisible(nowVisible);
        }

        void SetVisible(bool isVisible)
        {
            visible = isVisible;
            view.enabled = visible;
        }
    }
}