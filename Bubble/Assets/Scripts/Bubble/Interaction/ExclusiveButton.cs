using UnityEngine;

namespace Bubble.Interaction
{
    public class ExclusiveButton: Switchable
    {
        public Switchable switchable;
        public Switchable[] turnoffs;

        public override bool switched { get { return switchable.switched; } }

        public override void Switch(bool active)
        {
            switchable.Switch(active);
            if (active) {
                foreach (var turnoff in turnoffs)
                    turnoff.Switch(false);
            }
        }

        void OnDrawGizmos()
        {
            if (!switchable)
                return;
            Gizmos.color = Color.green;
            Gizmos.DrawLine(transform.position, switchable.transform.position);
            Gizmos.color = Color.red;
            foreach (var turnoff in turnoffs) {
                if (turnoff)
                    Gizmos.DrawLine(transform.position, turnoff.transform.position);
            }
        }
    }
}