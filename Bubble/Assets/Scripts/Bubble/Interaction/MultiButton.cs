using UnityEngine;

namespace Bubble.Interaction
{
    public class MultiButton: Switchable
    {
        public Switchable[] switchables;

        public override bool switched { get { return switchables[0].switched; } }

        public override void Switch(bool active)
        {
            foreach (var switchable in switchables)
                switchable.Toggle();
        }

        void OnDrawGizmos()
        {
            Gizmos.color = Color.green;
            foreach (var switchable in switchables) {
                if (switchable)
                    Gizmos.DrawLine(transform.position, switchable.transform.position);
            }
        }
    }
}