using UnityEngine;

namespace Bubble.Interaction
{
    using Nesting;

    public class ButtonContact: MonoBehaviour
    {
        public Material onMaterial;
        public Material offMaterial;
        public Material onGlow;
        public Material offGlow;

        Switchable button;
        Renderer view;
        Renderer glowView;

        bool switched;
        float cooldown = 1.0f;
        float coolingDown = 0.0f;

        void Awake()
        {
            var root = GetComponentInParent<Root>();
            button = root.GetComponentInChildren<Switchable>();
            view = GetComponent<Renderer>();
            glowView = transform.GetChild(0).GetComponent<Renderer>();
            view.material = (button.switched) ? onMaterial : offMaterial;
            switched = button.switched;
        }

        void OnCollisionEnter(UnityEngine.Collision collision)
        {
            var obj = collision.collider.gameObject;
            if (obj.layer == Layers.Creature || obj.layer == Layers.Bubble)
                Activate();
        }

        void Activate()
        {
            if (coolingDown <= 0.0f) {
                button.Toggle();
                coolingDown = cooldown;
            }
        }

        void Update()
        {
            if (coolingDown > 0.0f)
                coolingDown -= Time.deltaTime;
            if (button.switched != switched) {
                view.material = (button.switched) ? onMaterial : offMaterial;
                glowView.material = (button.switched) ? onGlow : offGlow;
                switched = button.switched;
            }
        }
    }
}