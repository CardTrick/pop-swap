﻿using UnityEngine;
using System.Collections;

public class ParticlePop : MonoBehaviour {

    ParticleSystem particles;

    bool firstTime = true;

    void Awake()
    {
        particles = GetComponent<ParticleSystem>();
    }

    void OnEnable()
    {
        if (firstTime) 
            firstTime = false;
        else
            Pop();
    }

    void Pop()
    {
        particles.Play();
    }

}
