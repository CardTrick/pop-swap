using UnityEngine;

namespace Bubble.Effects
{
    public class ScreenEffect: MonoBehaviour
    {
        public Material material;

        void OnRenderImage(RenderTexture src, RenderTexture dst)
        {
            Graphics.Blit(src, dst, material);
        }
    }
}