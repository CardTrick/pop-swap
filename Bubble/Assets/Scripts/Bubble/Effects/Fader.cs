﻿using UnityEngine;
using System.Collections;

namespace Bubble.Effects
{
    using Interaction;
    using Messaging;

    public class Fader: MonoBehaviour
    {
        public float fadeTime = 1.0f;

        Renderer ren;
        Material material;

        void Awake()
        {
            ren = GetComponentInChildren<Renderer>();
            material = ren.material;
        }

        void OnEnable()
        {
            MessageBus.Subscribe<LevelEndMessage>(OnLevelEnd);
        }

        void OnDisable()
        {
            MessageBus.Unsubscribe<LevelEndMessage>(OnLevelEnd);
        }

        void OnLevelEnd(LevelEndMessage message)
        {
            float timeScale = (message.successful) ? 1.0f : 0.5f;
            StartCoroutine(Fade(timeScale));
        }

        IEnumerator Fade(float timeScale)
        {
            float elapsed = 0.0f;
            var color = material.color;
            float endFadeTime = fadeTime * timeScale;
            while (elapsed < endFadeTime) {
                elapsed += Time.deltaTime;
                float t = elapsed / endFadeTime;
                color.a = t;
                material.color = color;
                ren.material = material;
                yield return null;
            }
            color.a = 1.0f;
            material.color = color;
            ren.material = material;
        }
    }
}