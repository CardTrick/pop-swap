﻿using UnityEngine;

namespace Bubble.Effects
{
    public class RenderToGlobalTexture: MonoBehaviour
    {
        public string textureName = "_ScreenEffect";

        void Awake()
        {
            var texture = new RenderTexture(Screen.width, Screen.height, 16);
            var cam = GetComponent<Camera>();
            cam.targetTexture = texture;
            Shader.SetGlobalTexture(textureName, texture);
        }
    }
}