﻿using UnityEngine;

namespace Bubble.Players
{
    public class BubbleAnimation: MonoBehaviour
    {
        public float animSpeed = 0.5f;
        public Vector2 animMagnitude;
        public float boingDecay = 50.0f;
        public float boing = 0.0f;
        float time = 0;

        public void Boing()
        {
            boing = 3.0f;
        }

        void Update()
        {
            if (boing > 0)
                boing = Mathf.Max(boing - Time.deltaTime*boingDecay, 0);
            time += Time.deltaTime;
            float t = 2 * Mathf.PI * time * (animSpeed + boing);
            float sx = Mathf.Sin(t) * animMagnitude.x;
            float sy = Mathf.Cos(t + 0.5f) * animMagnitude.y;
            transform.localScale = new Vector3(1.0f + sx, 1.0f + sy, 1.0f);
        }
    }
}