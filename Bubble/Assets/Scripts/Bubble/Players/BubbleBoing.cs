using UnityEngine;

namespace Bubble.Players
{
    using Nesting;
    
    public class BubbleBoing: MonoBehaviour
    {
        BubbleAnimation anim;
        AudioSource sound;

        void Wire()
        {
            var root = GetComponentInParent<Root>();
            anim = root.GetComponentInChildren<BubbleAnimation>();
            sound = GetComponentInChildren<AudioSource>();
        }

        void OnTriggerEnter()
        {
            anim.Boing();
            sound.Play();
        }
    }
}