using UnityEngine;
using System.Collections;

namespace Bubble.Players
{
    using Nesting;

    public class HumanPlayerFloat: MonoBehaviour
    {
        public float hoverTime = 0.5f;

        Rigidbody body;

        void Wire()
        {
            var root = GetComponentInParent<Root>();
            body = root.GetComponentInChildren<Rigidbody>();
        }

        void OnEnable()
        {
            if (body)
                Hover();
        }

        void Hover()
        {
            StartCoroutine(CoHover());
        }

        IEnumerator CoHover()
        {
            float elapsed = 0.0f;
            while (elapsed < hoverTime) {
                elapsed += Time.deltaTime;
                float t = elapsed / hoverTime;
                var floatAccel = Vector3.Lerp(-Physics.gravity, Vector3.zero, t);
                body.AddForce(floatAccel, ForceMode.Acceleration);
                yield return null;
            }
        }
    }
}