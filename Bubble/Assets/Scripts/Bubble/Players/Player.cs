﻿using UnityEngine;

namespace Bubble.Players
{
    using Shapeshifting;

    public enum PlayerIdentity
    {
        Invalid,
        FirstPlayer,
        SecondPlayer
    }

    public class Player: MonoBehaviour
    {
        public Material firstPlayerMaterial;
        public Material secondPlayerMaterial;

        public PlayerIdentity playerIdentity;

        public Material playerMaterial {
            get {
                return (playerIdentity == PlayerIdentity.SecondPlayer) ? secondPlayerMaterial : firstPlayerMaterial;
            }
        }

        void Wire()
        {
            Debug.Assert(playerIdentity != PlayerIdentity.Invalid);

            var formToggle = GetComponentInChildren<FormToggle>();
            formToggle.startWithSecondaryForm = (playerIdentity == PlayerIdentity.SecondPlayer);
        }
    }
}