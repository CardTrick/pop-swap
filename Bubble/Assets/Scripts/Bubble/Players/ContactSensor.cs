﻿using UnityEngine;

namespace Bubble.Players
{
    public class ContactSensor: MonoBehaviour
    {
        public LayerMask mask;
        public bool hasContact { get; private set; }

        bool ValidContact(Collider other)
        {
            if (other.isTrigger)
                return false;
            int layer = other.gameObject.layer;
            bool valid = (mask & (1 << layer)) > 0;
            return valid;
        }

        void OnTriggerStay(Collider other)
        {
            if (ValidContact(other))
                hasContact = true;
        }

        void OnTriggerExit(Collider other)
        {
            if (ValidContact(other))
                hasContact = false;
        }
    }
}