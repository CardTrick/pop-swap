﻿using UnityEngine;

namespace Bubble.Players
{
    using Nesting;
    
    public class PlayerColor: MonoBehaviour
    {
        Player player;

        void Wire()
        {
            var root = GetComponentInParent<Root>();
            player = root.GetComponentInChildren<Player>();
            var ren = GetComponentInChildren<Renderer>();
            ren.material = player.playerMaterial;
        }
    }
}
