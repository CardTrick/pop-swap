﻿using UnityEngine;

namespace Bubble.Players
{
    using Interaction;
    using Messaging;
    using Nesting;

    public class PlayerJumpControl: MonoBehaviour
    {
        public string jumpButton = "Jump";

        public float jumpSpeed = 16.0f;

        Rigidbody body;
        ContactSensor groundSensor;

        bool allowInput = true;

        void Wire()
        {
            var root = GetComponentInParent<Root>();
            body = root.GetComponentInChildren<Rigidbody>();
            groundSensor = root.GetComponentInChildren<ContactSensor>();
        }

        void OnEnable()
        {
            MessageBus.Subscribe<LevelEndMessage>(OnLevelEnd);
        }

        void OnDisable()
        {
            MessageBus.Unsubscribe<LevelEndMessage>(OnLevelEnd);
        }

        void OnLevelEnd(LevelEndMessage message)
        {
            allowInput = false;
        }

        void Update()
        {
            bool inputting = allowInput && Input.GetButtonDown("Jump");
            if (inputting && groundSensor.hasContact)
                Jump();
        }

        void Jump()
        {
            var vel = body.velocity;
            vel.y = jumpSpeed;
            body.velocity = vel;
        }
    }
}