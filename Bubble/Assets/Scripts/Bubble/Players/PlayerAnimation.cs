﻿using UnityEngine;

namespace Bubble.Players
{
    public class PlayerAnimation: MonoBehaviour
    {
        public float runAnimationSpeed = 2.0f;

        public float runningSpeed { get; set; }
        public bool running { get; set; }
        public bool grounded { get; set; }
        public bool movingLeft { get; set; }

        Animator anim;

        void Awake()
        {
            anim = GetComponentInChildren<Animator>();
        }

        void Update()
        {
            float runspeed = (running) ? runningSpeed / runAnimationSpeed : 1.0f;
            anim.SetFloat("RunningSpeed", runspeed);
            anim.SetBool("Grounded", grounded);
            anim.SetBool("Running", running);
            int direction = (movingLeft) ? -1 : 1;
            transform.localEulerAngles = new Vector3(0, direction*90, 0);
        }
    }
}