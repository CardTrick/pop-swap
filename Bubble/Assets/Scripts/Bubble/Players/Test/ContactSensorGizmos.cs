﻿using UnityEngine;

namespace Bubble.Players.Test
{
    public class ContactSensorGizmos: MonoBehaviour
    {
        Color color;

        void OnTriggerEnter(Collider other)
        {
            color = Color.green;
        }

        void OnTriggerExit(Collider other)
        {
            color = Color.red;
        }

        void OnDrawGizmosSelected()
        {
            var sphere = GetComponent<SphereCollider>();
            if (!sphere)
                return;

            Gizmos.color = color;
            Gizmos.DrawWireSphere(transform.position + sphere.center, sphere.radius*1.1f);
        }
    }
}