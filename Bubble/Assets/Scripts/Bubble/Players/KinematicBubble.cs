﻿using UnityEngine;

namespace Bubble.Players
{
    using Nesting;

    public class KinematicBubble: MonoBehaviour
    {
        public float bubbleMass = 0.1f;
        public float bubbleDrag = 10.0f;

        Rigidbody body;

        float baseMass;
        float baseDrag;

        void Wire()
        {
            var root = GetComponentInParent<Root>();
            body = root.GetComponentInChildren<Rigidbody>();
            baseMass = body.mass;
            baseDrag = body.drag;
            OnEnable();
        }

        void OnEnable()
        {
            if (body) {
                body.useGravity = false;
                body.mass = bubbleMass;
                body.drag = bubbleDrag;
            }
        }

        void OnDisable()
        {
            if (body) {
                body.useGravity = true;
                body.mass = baseMass;
                body.drag = baseDrag;
            }
        }
    }
}