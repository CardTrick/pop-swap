﻿using UnityEngine;

namespace Bubble.Players
{
    using Interaction;
    using Messaging;
    using Nesting;

    public class PlayerMovementControl: MonoBehaviour
    {
        const float MinimumSpeedThreshold = 0.05f;
        const float MinimumInputThreshold = 0.05f;

        public string inputAxis = "Horizontal";

        public float accelRate = 8.0f;
        public float decelRate = 8.0f;
        public float frictionRate = 8.0f;
        public float maxSpeed = 7.0f;

        public float aerialAccelRate = 4.0f;
        public float aerialDecelRate = 2.0f;
        public float aerialMaxSpeed = 5.0f;

        Rigidbody body;
        ContactSensor groundSensor;
        PlayerAnimation playerAnim;
        AudioSource sound;

        bool allowInput = true;
        bool wasGrounded;

        float input;
        bool inputting { get { return Mathf.Abs(input) > MinimumInputThreshold; } }
        int inputDirection { get { return (inputting) ? Mathf.RoundToInt(Mathf.Sign(input)) : 0; } }
        Vector3 velocity;
        float speed { get { return Mathf.Abs(velocity.x); } }
        bool moving { get { return speed > MinimumSpeedThreshold; } }
        int movementDirection { get { return (moving) ? Mathf.RoundToInt(Mathf.Sign(velocity.x)) : 0; } }


        void Wire()
        {
            var root = GetComponentInParent<Root>();
            body = root.GetComponentInChildren<Rigidbody>();
            groundSensor = root.GetComponentInChildren<ContactSensor>();
            playerAnim = root.GetComponentInChildren<PlayerAnimation>();
            sound = GetComponentInChildren<AudioSource>();
        }

        void OnEnable()
        {
            MessageBus.Subscribe<LevelEndMessage>(OnLevelEnd);
        }

        void OnDisable()
        {
            MessageBus.Unsubscribe<LevelEndMessage>(OnLevelEnd);
        }

        void OnLevelEnd(LevelEndMessage message)
        {
            allowInput = false;
        }

        void FixedUpdate()
        {
            input = (allowInput) ? Input.GetAxis(inputAxis) : 0;
            velocity = body.velocity;

            bool grounded = groundSensor.hasContact;
            playerAnim.grounded = grounded;
            if (grounded)
                GroundControl();
            else
                AerialControl();

            wasGrounded = grounded;
        }

        void GroundControl()
        {
            playerAnim.running = moving;
            playerAnim.runningSpeed = speed;
            if (!wasGrounded)
                PlayLandingSound();
            if (movementDirection != 0)
                playerAnim.movingLeft = movementDirection == -1;
            if (inputting)
                AccelOrDecel(accelRate, decelRate, maxSpeed);
            else if (moving)
                ApplyFriction();
            else
                StopMoving();
        }

        void AerialControl()
        {
            AccelOrDecel(aerialAccelRate, aerialDecelRate, aerialMaxSpeed);
        }

        void AccelOrDecel(float accel, float decel, float speedLimit)
        {
            bool reversing = movementDirection != inputDirection;
            if (moving && reversing)
                Decel(decel);
            else
                Accel(accel, speedLimit);
        }

        void Accel(float rate, float speedLimit)
        {
            var accel = Vector3.right * rate * input;
            bool belowSpeedLimit = speed < speedLimit;
            if (belowSpeedLimit)
                body.AddForce(accel, ForceMode.Acceleration);
        }

        void Decel(float rate)
        {
            var decel = Vector3.right * rate * input;
            body.AddForce(decel, ForceMode.Acceleration);
        }

        void ApplyFriction()
        {
            if (!wasGrounded) {
                StopMoving();
                return;
            }

            var decel = Vector3.right * frictionRate * -movementDirection;
            var nextVelocity = velocity + decel * Time.deltaTime;
            bool wouldChangeDirection = Vector3.Dot(velocity, nextVelocity) <= 0;

            if (wouldChangeDirection)
                StopMoving();
            else
                body.AddForce(decel, ForceMode.Acceleration);
        }

        void StopMoving()
        {
            var vel = body.velocity;
            vel.x = 0;
            body.velocity = vel;
        }

        void PlayLandingSound()
        {
            sound.Play();
        }
    }
}