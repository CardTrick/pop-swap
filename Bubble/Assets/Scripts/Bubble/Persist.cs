﻿using UnityEngine;

namespace Bubble
{
    public class Persist: MonoBehaviour
    {
        void Awake()
        {
            DontDestroyOnLoad(gameObject);
        }
    }
}