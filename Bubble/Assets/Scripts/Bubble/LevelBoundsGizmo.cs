﻿using UnityEngine;

namespace Bubble
{
    public class LevelBoundsGizmo: MonoBehaviour
    {
        static Vector3 LevelSize = new Vector3(32.0f, 18.0f, 1.0f);

        void OnDrawGizmos()
        {
            Gizmos.color = Color.grey;
            Gizmos.DrawWireCube(Vector3.zero, LevelSize);
        }
    }
}