﻿Shader "Bubble/BlurEffect"
{
    Properties
    {
        _MainTex ("Main texture", 2D) = "white" {}
        _BlurRadius ("Blur Radius (XY)", Vector) = (5, 0, 0, 0)
    }
    SubShader
    {
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            sampler2D _MainTex;
            float2 _BlurRadius;

            struct appdata
            {
                float4 vertex: POSITION;
                float2 uv: TEXCOORD0;
            };

            struct trans
            {
                float4 vertex: SV_POSITION;
                float2 uv: TEXCOORD0;
            };

            trans vert(appdata v)
            {
                trans t;
                t.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
                t.uv = v.uv;
                return t;
            }

            fixed4 frag(trans i): SV_Target
            {
            	float2 blurOffset = _BlurRadius.xy / (float2(1600.0, 900.0));
                float gaussians[15] = {
                    0.0045533429216401775,
                    0.011524014780679824,
                    0.02528338310132549,
                    0.04808664695042801,
                    0.07928147766381809,
                    0.11331226170638055,
                    0.14039124046868956,
                    0.15078600877302686,
                    0.14039124046868956,
                    0.11331226170638055,
                    0.07928147766381809,
                    0.04808664695042801,
                    0.02528338310132549,
                    0.011524014780679824,
                    0.0045533429216401775
                };

                float4 total = float4(0, 0, 0, 0);
                float2 centre = i.uv;
                for (int x=0; x<15; ++x) {
                    float2 uv = centre + float2((x-7), (x-7)) * blurOffset;
                    float4 blurSample = tex2D(_MainTex, uv);
                    total += blurSample * gaussians[x];
                }
                total.a = tex2D(_MainTex, centre).a;

                return total;
            }
            ENDCG
        }
    }
}
