Shader "Bubble/GlowEffect"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Intensity ("Intensity", Float) = 1.0
    }
    SubShader
    {
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            sampler2D _MainTex;
            sampler2D _GlowMap;
            float _Intensity;

            struct appdata
            {
                float4 vertex: POSITION;
                float2 uv: TEXCOORD0;
            };

            struct trans
            {
                float4 vertex: SV_POSITION;
                float2 uv: TEXCOORD0;
            };

            trans vert(appdata v)
            {
                trans t;
                t.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
                t.uv = v.uv;
                return t;
            }

            fixed4 frag(trans i): SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv);
                float2 guv = i.uv;
                #ifdef UNITY_UV_STARTS_AT_TOP
                guv.y = 1 - guv.y;
                #endif
                fixed4 glow = tex2D(_GlowMap, guv);
                glow *= _Intensity * step(glow.a, 0.5);
                return fixed4(col.rgb - glow.rgb, 1.0);
            }
            ENDCG
        }
    }
}
